#!/usr/bin/env bash

#
# Parameters:
# SSL_CERT_ROOT_DIR   Location where SSL certificates are stored, defaults to /etc/nginx/ssl
#
# BOOTSTRAP_SSL       0: SSL certificates are managed outside this container.
#                        Block starting nginx until "${_SSL_CERT_ROOT_DIR}/fullchain.pem" exists or is created.
#                     1 (default): Create self signed SSL certificates or copy over the provided certificates (server.crt
#                        and server.key) from /nginx_ssl.
#
# Certificate and private key location within the container:
#   "${_SSL_CERT_ROOT_DIR}/fullchain.pem"
#   "${_SSL_CERT_ROOT_DIR}/privkey.pem"
#

_SSL_CERT_ROOT_DIR=${SSL_CERT_ROOT_DIR:-"/etc/nginx/ssl"}
_BOOTSTRAP_SSL=${BOOTSTRAP_SSL:-"1"}

generate_self_signed_certificate() {
  cd /tmp || exit 1
  openssl genrsa -des3 -passout 'pass:xxxxx' -out 'server.pass.key' 2048
  openssl rsa -passin 'pass:xxxxx' -in 'server.pass.key' -out 'server.key'
  rm 'server.pass.key'
  openssl req -new -key 'server.key' -out 'server.csr' -subj "/CN=example.com"
  openssl x509 -req -days 365 -in 'server.csr' -signkey 'server.key' -out 'server.crt'
  mv 'server.crt' "${_SSL_CERT_ROOT_DIR}/fullchain.pem"
  mv 'server.key' "${_SSL_CERT_ROOT_DIR}/privkey.pem"
}

initialize() {
    echo "Initializing nginx container"

    if [ "${_BOOTSTRAP_SSL}" == "0" ]; then
      echo "External ssl enabled"
      if [ ! -f "${_SSL_CERT_ROOT_DIR}/fullchain.pem" ]; then
        echo "Waiting for certificates"
        inotifywait --exclude .swp -e create -e modify -e delete -e move "${_SSL_CERT_ROOT_DIR}"
      fi
    else
      echo "External ssl disabled, generating self signed SSL certificate if needed"
      if [ ! -f "${_SSL_CERT_ROOT_DIR}/fullchain.pem" ] || [ ! -f "${_SSL_CERT_ROOT_DIR}/privkey.pem" ]; then
          if [ ! -d '/nginx_ssl.d' ]; then
              echo "No SSL certificate found for nginx. Generating new certificate."
              generate_self_signed_certificate
          else
              # Copy supplied SSL certificate
              echo "Copying supplied SSL certificate for nginx."
              cp '/nginx_ssl.d/server.crt' "${_SSL_CERT_ROOT_DIR}/fullchain.pem"
              cp '/nginx_ssl.d/server.key' "${_SSL_CERT_ROOT_DIR}/privkey.pem"
          fi
      fi
      chmod 0600 '/etc/nginx/ssl/privkey.key'
    fi

    if [ -d '/nginx_conf.d' ] && [ ! "$(find /etc/nginx/conf.d/ -name \*.conf -type f  | wc -l)" -gt 0 ]; then
        # Copy startup supplied configuration on fresh start
        echo "Copying nginx supplied extra configuration files."
        cp -r /nginx_conf.d/* '/etc/nginx/conf.d/'
        chown -R nginx:nginx '/etc/nginx/conf.d'
    fi

}

rm -f /var/run/rsyslogd.pid
initialize
/nginx_reloader.bash > /var/log/nginx_reloader.log 2>&1 &
