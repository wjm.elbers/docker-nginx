#!/usr/bin/env bash

_SSL_CERT_ROOT_DIR=${SSL_CERT_ROOT_DIR:-"/etc/nginx/ssl"}

echo "$(date +"%Y-%m-%d %T"): Watching ${_SSL_CERT_ROOT_DIR} for changes"

while true
do
 inotifywait --recursive --exclude .swp --exclude privkey.pem -e create -e modify -e delete -e move "${_SSL_CERT_ROOT_DIR}"
 #nginx -t
 #shellcheck disable=SC2181
 #if [ $? -eq 0 ]
 if nginx -t; then
  echo "$(date +"%Y-%m-%d %T"): Detected Nginx Certificate Update"
  echo "$(date +"%Y-%m-%d %T"): Executing: nginx -s reload"
  nginx -s reload
 fi
done