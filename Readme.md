
# SSL support

## Bootstrapping certificates
* If `EXTERNAL_SSL=1`, startup will wait for certificates to become available in `/etc/nginx/ssl`.
  * Docker image to manage SSL certificates: https://gitlab.com/wjm.elbers/docker-certbot
* If `EXTERNAL_SSL` does not exist or `EXTERNAL_SSL=0`, a self signed certificate will be generated on startup.

## Renewing of certificates
Certificates are expected under `/etc/nginx/ssl/*`, subdirectories are supported. Any change to certificate files under
this directory will trigger a reload of nginx if `EXTERNAL_SSL=1` is set. Any private key files (named `privkey.pem`) are
excluded.

#Example

## nginx vhost config

`./nginx/default.conf` example:
```
server {
       listen           80;
       server_name      _;
       return           301 https://localhost:9443$request_uri;
}

server {
    listen              443 ssl;
    server_name         _;
    ssl_certificate     /etc/nginx/ssl/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/privkey.pem;
}

```

## Option 1: Local SSL management

`docker-compose.yml` example:
```
version: '3.6'
services:
  proxy:
    image: docker-nginx:76f6c52
    volumes:
      - ./nginx/default.conf:/etc/nginx/conf.d/default.conf:ro
    ports:
      - 9080:80
      - 9443:443
```

## Option 2: Externl SSL management
`docker-compose.yml` example:
```
version: '3.6'
services:
  proxy:
    image: docker-nginx:76f6c52
    environment:
      - EXTERNAL_SSL: 1
    volumes:
      - ./nginx/default.conf:/etc/nginx/conf.d/default.conf:ro
      - certbot-shared-tls:/etc/nginx/ssl
    ports:
      - 9080:80
      - 9443:443

  certbot:
    image: docker-certbot:a7ff2f4
    environment:
      - NGINX_HOSTNAME=proxy
    volumes:
      - certbot-shared-tls:/shared

volumes:
  certbot-shared-tls:
```
# References
* [How-to: Auto Reload Nginx For Dynamic Services](https://cyral.com/blog/how-to-auto-reload-nginx/)